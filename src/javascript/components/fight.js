import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination } = controls

    const firstFighterInitialHealth = firstFighter.health
    const secondFighterInitialHealth = secondFighter.health
    const firstFighterBlockHealth = document.querySelector('#left-fighter-indicator')
    const secondFighterBlockHealth = document.querySelector('#right-fighter-indicator')

    onPressingKeyCombination(
      () => getFighterHealth(
        firstFighter,
        secondFighter,
        getDamage,
        secondFighterBlockHealth,
        secondFighterInitialHealth
      ),
      PlayerOneAttack, PlayerTwoBlock
    )

    onPressingKeyCombination(
      () => getFighterHealth(
        firstFighter,
        secondFighter,
        getHitPower,
        secondFighterBlockHealth,
        secondFighterInitialHealth
      ),
      PlayerOneAttack
    )

    onPressingKeyCombination(
      () => getFighterHealth(
        secondFighter,
        firstFighter,
        getDamage,
        firstFighterBlockHealth,
        firstFighterInitialHealth
      ),
      PlayerTwoAttack, PlayerOneBlock
    )

    onPressingKeyCombination(
      () => getFighterHealth(
        secondFighter,
        firstFighter,
        getHitPower,
        firstFighterBlockHealth,
        firstFighterInitialHealth
      ),
      PlayerTwoAttack
    )

    onPressingKeyCombination(
      throttle(() => getFighterHealth(
        firstFighter,
        secondFighter,
        getCriticalHitPower,
        secondFighterBlockHealth,
        secondFighterInitialHealth
      ), 10000),
      PlayerOneCriticalHitCombination[0],
      PlayerOneCriticalHitCombination[1],
      PlayerOneCriticalHitCombination[2],
    );

    onPressingKeyCombination(
      throttle(() => getFighterHealth(
        secondFighter,
        firstFighter,
        getCriticalHitPower,
        firstFighterBlockHealth,
        firstFighterInitialHealth
      ), 10000),
      PlayerTwoCriticalHitCombination[0],
      PlayerTwoCriticalHitCombination[1],
      PlayerTwoCriticalHitCombination[2],
    );

    function getFighterHealth(attaker, defender, callback, fighterBlockHealth, fighterInitialHealth) {
      defender.health -= callback(attaker, defender)
      // console.log('Defender.health: ', defender.health)
      fighterBlockHealth.style.width = `${getBlockWidth(defender.health, fighterInitialHealth)}%`
      if (defender.health <= 0) {
        resolve(attaker)
      }
    }

    function onPressingKeyCombination(func, ...codes) {
      let pressed = new Set();
      document.addEventListener('keydown', function (e) {
        pressed.add(e.code);
        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();
        func();
      });
      document.addEventListener('keyup', function (e) {
        pressed.delete(e.code);
      });
    }

    function throttle(func, timeout) {
      let lastCall = 0;
      return function () {
        let currentCall = new Date();
        if (currentCall - lastCall >= timeout) {
          func();
          lastCall = currentCall;
        }
      };
    }

    function getCriticalHitPower(fighter) {
      return fighter.attack * 2
    }

    function getBlockWidth(currentHealth, initialHealth) {
      const currentBlockWidth = currentHealth / initialHealth * 100
      return currentBlockWidth > 0 ? currentBlockWidth : 0
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender)
  // console.log('Damage: ', damage > 0 ? damage : 0)
  return damage > 0 ? damage : 0
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter
  const criticalHitChance = Math.random() * (2 - 1) + 1
  // console.log('HitPower: ', attack * criticalHitChance)
  return attack * criticalHitChance
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter
  const dodgeChance = Math.random() * (2 - 1) + 1
  // console.log('BlockPower: ', defense * dodgeChance)
  return defense * dodgeChance
}